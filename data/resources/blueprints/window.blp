using Gtk 4.0;
using Adw 1;

template $AppWindow : Adw.ApplicationWindow {
  default-width: 500;
  default-height: 600;
  width-request: 360;
  height-request: 294;
  title: _("Impression");

  Stack main_stack {
    transition-type: crossfade;

    StackPage {
      name: "choose";

      child:
      Adw.NavigationView navigation {
        Adw.NavigationPage {
          tag: "welcome";
          title: _("Impression");

          child:
          Adw.ToolbarView {
            [top]
            Adw.HeaderBar {
              [end]
              MenuButton {
                icon-name: "open-menu-symbolic";
                menu-model: primary_menu;
                tooltip-text: _("Main Menu");
                primary: true;
              }
            }

            content:
            Box {
              hexpand: true;
              vexpand: true;
              orientation: vertical;
              spacing: 12;
              Adw.PreferencesPage {
                Adw.PreferencesGroup {
                  Image {
                    icon-name: "io.gitlab.adhami3310.Impression";
                    
                    styles ["app_icon"]
                  }
                  Label {
                    label: _("Choose Image");

                    styles ["title", "title-1"]
                  }

                  margin-bottom: 12;
                }

                Adw.PreferencesGroup {
                  Adw.ActionRow open_image_button {
                    title: _("Open File…");
                    activatable-widget: next_icon;

                    Image next_icon {
                      icon-name: 'go-next-symbolic';
                    }
                  }
                }

                Adw.PreferencesGroup {
                  title: _("Direct Download");

                  [header-suffix]
                  DropDown architecture {
                    model: StringList {
                      strings ["x86_64", "aarch64"]
                    };

                    sensitive: false;
                    
                    styles ["architecture"]
                  }

                  Box {
                    orientation: vertical;

                    Box download_spinner {
                      orientation: vertical;

                      Spinner {
                        valign: center;
                        vexpand: true;
                        height-request: 32;
                        width-request: 32;
                        spinning: true;
                      }
                      styles ["card", "loading-frame"]
                    }

                    Box offline_screen {
                      orientation: vertical;
                      visible: false;

                      Adw.StatusPage {
                        title: _("No Connection");
                        description: _("Connect to the internet to view available images");

                        styles  ["dim-label"]
                      }
                      styles ["card"]
                    }
                    Box distros {
                      visible: false;
                      orientation: vertical;
                      ListBox amd_distros {
                        selection-mode: none;
                        styles ["boxed-list"]
                      }

                      ListBox arm_distros {
                        visible: false;
                        selection-mode: none;
                        styles ["boxed-list"]
                      }
                    }

                  }
                }
              }
            }

            ;
          }

          ;
        }

        Adw.NavigationPage {
          tag: "device_list";
          title: _("Impression");

          child:
          Adw.ToolbarView {
            [top]
            Adw.HeaderBar {
              [end]
              MenuButton {
                icon-name: "open-menu-symbolic";
                menu-model: primary_menu;
                tooltip-text: _("Main Menu");
                primary: true;
              }
            }
            
            content:
            Adw.PreferencesPage {
              valign: center;

              Adw.PreferencesGroup {
                Box {
                  orientation: vertical;
                  spacing: 12;

                  Image {
                    icon-name: "media-optical-cd-symbolic";
                    pixel-size: 64;
                    styles ["dim-label"]
                  }

                  Label name_value_label {
                    label: "arch.iso";
                    wrap: true;
                    wrap-mode: word_char;
                    lines: 3;
                    justify: center;
                    max-width-chars: 24;

                    styles [
                      "title-3",
                    ]
                  }

                  Label size_label {
                    justify: center;

                    styles [
                      "caption",
                    ]
                  }
                }
              }

              Adw.PreferencesGroup {

                Adw.Clamp {
                  maximum-size: 450;
                  tightening-threshold: 200;

                  ListBox available_devices_list {
                    selection-mode: none;

                    styles [
                      "boxed-list",
                    ]
                  }
                }
              }

              Adw.PreferencesGroup {
                valign: end;
                halign: center;
                description: _("All data on the selected drive will be erased");

                Button flash_button {
                  label: _("Write");
                  sensitive: false;
                  halign: center;

                  styles [
                    "destructive-action",
                    "pill",
                  ]
                }
              }
            }

            ;
          }

          ;
        }
      }

      ;
    }

    StackPage {
      name: "status";

      child:
      Adw.ToolbarView {
        [top]
        Adw.HeaderBar {
          [end]
          MenuButton {
            icon-name: "open-menu-symbolic";
            menu-model: primary_menu;
            tooltip-text: _("Main Menu");
            primary: true;
          }
        }

        content:
        Adw.ToastOverlay toast_overlay {
          child:
          Stack stack {
            transition-type: crossfade;

            StackPage {
              name: "loading";
              child:
                  Spinner loading_spinner {
                    valign: center;
                    halign: center;
                    height-request: 28;
                    width-request: 28;
                  }

              ;
            }

            StackPage {
              name: "no_devices";
              child:
              Adw.StatusPage {
                icon-name: "io.gitlab.adhami3310.Impression";
                title: _("No Drives");
                description: _("Insert a drive to write to");
              }

              ;
            }

            StackPage {
              name: "success";
              child: 
              Adw.StatusPage {
                icon-name: "check-round-outline-symbolic";
                title: _("Writing Completed");
                description: _("The drive can be safely removed");
                child:
                Box {
                  orientation: vertical;
                  spacing: 12;

                  Button done_button {
                    valign: center;
                    halign: center;
                    label: _("Finish");
                    use-underline: true;

                    styles [
                      "suggested-action",
                      "pill",
                    ]
                  }
                }

                ;
              }

              ;
            }

            StackPage {
              name: "failure";
              child: 
              Adw.StatusPage {
                icon-name: "error-symbolic";
                title: _("Writing Unsuccessful");
                child:
                Box {
                  orientation: vertical;
                  spacing: 12;

                  Button try_again_button {
                    valign: center;
                    halign: center;
                    label: _("Retry");
                    use-underline: true;

                    styles [
                      "suggested-action",
                      "pill",
                    ]
                  }
                }

                ;
              }

              ;
            }

            StackPage {
              name: "flashing";
              child:
              Adw.StatusPage flashing_page {
                title: _("Writing…");
                icon-name: "flash-symbolic";
                description: _("Do not remove the drive");

                Box {
                  orientation: vertical;
                  spacing: 36;

                  ProgressBar progress_bar {
                    valign: center;
                    halign: center;
                    show-text: true;
                  }

                  Button cancel_button {
                    valign: center;
                    halign: center;
                    label: _("_Cancel");
                    use-underline: true;

                    styles [
                      "pill",
                    ]
                  }
                }
              }

              ;
            }
          }

          ;
        }

        ;
      }

      ;
    }
  }
}

menu primary_menu {
  section {
    item {
      label: _("Keyboard Shortcuts");
      action: "win.show-help-overlay";
    }

    item {
      label: _("About Impression");
      action: "win.about";
    }
  }
}
